# Group 20 ENERGY DATA

Repository for the AdPro group assignment of Group 20.

## Description
Our group of Master Students are analysing the energy mix of several countries, to derrive insights of the worlds energy consumption and contribute our part to a greener future.

We have created the class "EnergyData", which helps to get a better insight in the worlds energy consumption.

EnergyData is a class that can be used to analyze energy data from 1970 to 2021. It contains a broad functionality to visualize past energy consumption and CO2 emissions for user-specified countries and years. It also has built-in functionality to predict future values for emissions and consumptions.

## Installation 
To create the environment in which our project runs, please go in the directory of this project and run the following code
```
conda env create -f energy_environment.yml
```
afterwards activate it by running
```
conda activate energy
```
you will now have an environment with the required packages for running our EnergyData class


## Support
If there are any open questions or problems, feel free to join the official whatsapp group chat of group20 via this [link](https://chat.whatsapp.com/IoRWGecAKTaJjYHjqOd5Cp). This is a safe space to ask anything regarding this group project or to simply get exclusive insights about the process that was behind the project. 

## Authors and acknowledgment
The authors of this project are:<br>
Sherwin Husseinifard (50568@novasbe.pt)<br>
Sören Böhling (48694@novasbe.pt)<br>
Simon Link (simon.link@novasbe.pt)<br>
Tim Ilbertz (48809@novasbe.pt)<br>

A special acknowledgement goes out to Hannah Ritchie, Pablo Rosado, Edouard Mathieu and Max Roser for collecting, aggregating, and documenting the Data on Energy by Our World in Data.<br>

Big shotout to Underworld and their timeless masterpiece ["Second Thoughest In The Infants"](https://open.spotify.com/album/2L4lA4O95iu8bvq5Nzo6aI?si=SsDrDOk6SsmbZmrvA_G62w) for providing us with the needed energy and motivation to finish this project.


## License

All visualizations, data, and code produced by us are completely open access under the [MIT](https://choosealicense.com/licenses/mit/) license. You have the permission to use, distribute, and reproduce these in any medium, provided the source and authors are credited. <br>
The data produced by third parties and made available by Our World in Data is subject to the license terms from the original third-party authors. We will always indicate the original source of the data in our database, and you should always check the license of any such third-party data before use.



