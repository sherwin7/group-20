.. Energy Data Group 20 documentation master file, created by
   sphinx-quickstart on Thu Mar 17 19:16:42 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Energy Data Group 20's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
