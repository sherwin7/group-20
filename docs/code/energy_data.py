"""
This class helps to get a better insight in the worlds energy consumption

Classes:
    EnergyData : A class that can be used to analyze energy data from 1970 to
    2021. Contains a broad functionality to visualize past energy consumption
    and CO2 emissions for user-specified countries and years. It also has
    built-in functionality to predict future values for emissions and
    consumptions
"""

import os
from typing import Union
import pandas as pd
import numpy as np
from statsmodels.tsa.arima.model import ARIMA
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches


class EnergyData:
    """
    This class is the blueprint for all objects that are created to inspect
    the data set

    Attributes:
        data_frame (pd.DataFrame): A data frame containing the energy data set

    """

    def __init__(self):
        """
        This method initializes the data frame attribute of the class that
        stores the dataframe and all the data used.
        """

        # Dataframe objects are an empty list for a start
        self.data_frame = []
        self.data_frame_filtered = []

    def download(self) -> None:
        """
        This method checks if the dataset is already downloaded, and if not,
        downloads the data set from the Web Source.
        Furthermore, it filters for only years after 1970, inclusively.
        """

        try:
            data_frame = pd.read_csv("downloads/data.csv")
            self.data_frame = data_frame[data_frame['year'] >= 1970]
        except Exception:
            url = "https://raw.githubusercontent.com/owid/energy-data/master/owid-energy-data.csv"
            data_frame = pd.read_csv(url)
            path = "downloads"
            os.mkdir(path)
            data_frame.to_csv("downloads/data.csv")

            self.data_frame = data_frame[data_frame['year'] >= 1970]
        finally:
            pd.options.mode.chained_assignment = None

            self.data_frame['year'] = pd.to_datetime(self.data_frame['year'],
                                                     format='%Y')
            self.data_frame.set_index('year', inplace=True)
            self.data_frame_filtered = self.\
                data_frame.loc[:, ~self.data_frame.columns.isin([
                    'renewables_consumption', 'low_carbon_consumption',
                    'primary_energy_consumption', 'fossil_fuel_consumption'])]

            emissions = {
                'biofuel': 1450,
                'coal': 1000,
                'gas': 455,
                'hydro': 90,
                'nuclear': 5.5,
                'oil': 1200,
                'solar': 53,
                'wind': 14
            }

            def calculate_emissions(row_wise):

                ems = []

                for source, emission in emissions.items():
                    con = row_wise[source + "_consumption"]

                    source_emission = (con * 1000000000) * (emission / 1000000)
                    #  In tonns?
                    #  source_emission = (con * 1000) * (emission / 1000000)

                    if not np.isnan(source_emission):
                        ems.append(source_emission)

                emission_total = sum(ems)
                return emission_total

            self.data_frame['emissions'] = self.data_frame.\
                apply(calculate_emissions, axis=1)
            self.data_frame_filtered['emissions'] = self.data_frame.\
                apply(calculate_emissions, axis=1)

    def list_countries(self) -> list:
        """
        Lists all available countries in the data set.

        Returns
        -------
        list
            The list of all countries available in the dataset.

        """
        unique_countries = set(self.data_frame.country)
        return list(unique_countries)

    def plot_consumption(self, countries: Union[str, list],
                         normalize: bool = True):
        """
        Plot a stacked area chart of the energy consumption of the choosen
        country broken down by its source.
        If normalize set True all values will be normalized as percentage of
        Total consumption in the respective year.

        Parameters
        ----------
        countries : list, str
            Desired countries to be plotted
        normalize : bool
            If set to True, values will be normalized as percentage of total
            consumption in respective year

        """

        # Checking if the input countries  are (list or string),  normalize
        # value (boolean) and if the country is in the known in the DF
        # Otherwhise it should give Type- or ValueError

        if not isinstance(countries, (list, str)):
            raise TypeError("Input Variable is not string or list.")
        if isinstance(countries, str):
            countries = [countries]
        if normalize not in [True, False]:
            raise TypeError("Normalize Argument must be Boolean")

        # Now we iterate through each country in the list and create one (!) 
        # individual plot

        for i in countries:
            if i not in set(self.data_frame_filtered.country):
                raise ValueError("Choosen Country not in the list")

            # Here we create seperate dataframe per country with only the
            # coloumns regarding energy consumption and sum the valuesof the
            # columns to compute the total energy consumption
            df1 = self.data_frame_filtered[
                    self.data_frame_filtered["country"] == i]\
                .filter(regex="consumption")
            df1.index = self.data_frame_filtered[
                self.data_frame_filtered["country"] == i].index
            df1 = df1.dropna()

            # Renaming the columns to make the legend more readable
            columns = ["Biofuel", "Coal", "Gas", "Hydro", "Nuclear", "Oil",
                       "Other Renewables", "Solar", "Wind"]
            oldC = df1.columns
            newC = ["Biofuel", "Coal", "Gas", "Hydro", "Nuclear", "Oil",
                    "Other Renewables", "Solar", "Wind"]
            res = dict(zip(oldC, newC))
            df1.rename(columns=res, inplace=True)

            # In case normalize was set True we divide every singe value by the
            # total consumption to compute the fraction of the
            # respective energy source of the Total consumption
            if normalize is False:
                y_label = "Energy consumption in terawatt-hours"
            else:
                total_consumption = df1.sum(axis=1)
                df1 = df1.div(total_consumption, axis=0)
                df1 = df1*100
                y_label = "Percentage of Total Energy consumption"

            # Plotting the data
            fig, axe = plt.subplots(figsize=(20, 10))
            fig = df1.plot.area(figsize=(20, 10), ax=axe)
            plt.xlabel(xlabel="Year", fontsize=16)
            plt.ylabel(ylabel=y_label, fontsize=16)
            fig.legend(bbox_to_anchor=(1.1, 1.05), fontsize=16)
            fig.set_title(f" Sources of {i}'s energy Consumption from 1970 to 2020", fontsize=22)
            plt.show()

    def compare_consumption(self, countries: Union[str, list]) -> None:
        """
        Takes a list or string, sums all energy consumtion data and displays a
        dataframe with columns of choosen countries and the type of energy
        consumtion as index.

        Parameters
        ----------
        countries : list, str
            Desired countries to be plotted

        """

        # Here we check if the input countries are (1) a string or a list and
        # (2) if the countries are in the given in the OG dataframe
        if not isinstance(countries, (list, str)):
            raise TypeError("Input Variable is not string or list.")
        if isinstance(countries, str):
            countries = [countries]
        for i in countries:
            if i not in set(self.data_frame_filtered.country):
                raise ValueError("Choosen Country not in the list")

        # Here we create a list of colors with the same length of the countries
        # to assign a color to each country
        color = plt.cm.rainbow(np.linspace(0, 1, len(countries)))

        fig = plt.figure(figsize=(20, 10))
        ax1 = fig.add_subplot()
        ax2 = ax1.twinx()

        c_names = ""

        # Here we iterate through the dictionary of conuntires as keys anc
        # color as avalues
        for i, colors in zip(countries, color):

            # Computing total_consumption per country
            total_consumption = self.data_frame_filtered[
                (self.data_frame_filtered.index.year < 2020) &
                (self.data_frame_filtered["country"] == i)]\
                .filter(regex="consumption").sum(axis=1)

            # Computing total_emission per country
            total_emission = self.data_frame[
                (self.data_frame.index.year < 2020) &
                (self.data_frame["country"] == i)]["emissions"]

            # Plotting total_consumption
            ax1.plot(self.data_frame_filtered[
                    (self.data_frame_filtered.index.year < 2020) &
                    (self.data_frame_filtered["country"] == i)]
                    .index.year, total_consumption, label=i, color=colors)

            # Plotting total_emission
            ax2.plot(self.data_frame[
                    (self.data_frame.index.year < 2020) &
                    (self.data_frame["country"] == i)]
                    .index.year, total_emission, label=i,
                    color=colors, linestyle='--')
            c_names = c_names + i + ", "

        # creating a string containing the countries seperated by a "," or and
        # and "and between the last two
        last_char_index = c_names.rfind(",")
        c_names = c_names[:last_char_index]
        last_char_index = c_names.rfind(",")
        c_names = c_names[:last_char_index] + " and" +\
            c_names[last_char_index+1:]

        plt.title(f'Comparison of Total Energy Consumption between {c_names}',
                  fontsize=22)

        # Creating approptiate legends
        h_1, l_1 = ax1.get_legend_handles_labels()
        h_2, l_2 = ax2.get_legend_handles_labels()
        ax1.legend(handles=h_1, labels=l_1, loc='upper left', fontsize=11)
        ax2.legend(handles=h_2, labels=l_2, loc='upper right', fontsize=11)

        ax1.set_ylabel('Energy Consumption in TWh', fontsize=16)
        ax2.set_ylabel('CO2 Emissions in Tonnes', fontsize=16)
        ax1.set_xlabel('Years', fontsize=16)
        plt.show()

    def compare_gdp(self, countries: Union[str, list]) -> None:
        """
        Compares the GDP of different countries in a plotted graph.

        Parameters
        ----------
        countries : list, str
            Desired countries to be plotted
        """
        if isinstance(countries, str):
            countries = [countries]
        plt.figure(figsize=(20, 10), dpi=80)
        c_names = ""

        # Iterating over the input country list and plotting the GDP per year
        for i in countries:
            plt.plot(self.data_frame[self.data_frame["country"] == i]
                     .index.year, self.data_frame[self.data_frame[
                         "country"] == i]["gdp"], label=i)
            c_names = c_names + i + ", "

        # Creating a string containing the countries seperated by a "," or and
        # and "and between the last two
        last_char_index = c_names.rfind(",")
        c_names = c_names[:last_char_index]
        last_char_index = c_names.rfind(",")
        c_names = c_names[:last_char_index] + " and" + c_names[
            last_char_index+1:]

        plt.xlabel("Years", fontsize=16)
        plt.ylabel("GDP in trillion", fontsize=16)
        plt.legend(fontsize=16)
        plt.title(f"GDP of {c_names}", fontsize=22)
        plt.show()

    def gapminder(self, year: int) -> None:
        """
        Takes a year as input and creates a scatterplot with X as GDP,
        Y as total energy consumption and size as relative population.

        Parameters
        ----------
        year : int
            Desired year that the plot should be created for
        """

        if not isinstance(year, (int)):
            raise TypeError("Input variable must be an interger.")
        if (year > 2016) or (year < 1970):
            raise ValueError("Not data available for that year")

        # Aim is to match each country to a continent to assign each bubble in
        # the gapminder a color corresponding to the continent for that we
        # downloaded a dataset with the contintens per country and merged it
        # with our orignal on on ISO3 codes to create a new df
        url = "https://raw.githubusercontent.com/datawookie/data-diaspora/master/spatial/country-continent-codes.csv"
        data_frame = pd.read_csv(url,  header=1)
        df_cd = pd.merge(self.data_frame_filtered.reset_index(), data_frame,
                         how='left', left_on='iso_code', right_on='iso3')
        df_cd['year'] = pd.to_datetime(df_cd['year'], format='%Y')
        df_cd.set_index('year', inplace=True)

        # Raise an error if there is not sufficient data in choosen year
        if df_cd['gdp'].sum() == 0:
            raise Exception(f"For picked year {year} is not sufficient data\
                            available")

        # From the new dataset we drop rows not corrospinding to
        # "real countries" (e.g. world or Opec)
        df_cd = df_cd[(df_cd.index.year == year) &
                      (df_cd['country_x'] != 'World')]
        df_cd["continent"].fillna('Other', inplace=True)
        df_cd = df_cd[df_cd['continent'] != 'Other']

        # Adapt df to create coloumn for total consumption and normalize the
        # population to make it easier to use values as size in scatterplot
        df_cd["Total_Consumption"] = df_cd.filter(
            regex="consumption").sum(axis=1)

        df_cd["area"] = (df_cd['population'] /
                         df_cd['population'].sum())*100000

        # Create a dictionary with continets as keys and colors as values to
        # uniform legend values and colors in scatterplot
        continents = sorted(set(df_cd["continent"]), key=str.lower)
        colors = ['blue', 'green', 'purple', 'red', 'pink', 'orange']
        res = dict(zip(continents, colors))

        # Create scatterplot by iterating through continents
        fig, axe = plt.subplots()
        for i, j in res.items():
            axe.scatter(df_cd[df_cd["continent"] == i]["gdp"], df_cd[
                df_cd["continent"] == i]["Total_Consumption"],  label=i,
                alpha=0.5, s=df_cd[df_cd["continent"] == i]["area"],
                linewidths=2, c=j)

        axe.set_xscale("log")
        axe.set_yscale("log")
        axe.set_ylim(top=df_cd["Total_Consumption"].max()*6)
        axe.set_xlim(left=df_cd["gdp"].min())

        # Create legend handles
        handles_c = []
        for i, j in res.items():
            patch = mpatches.Patch(color=j, label=i, alpha=0.5)
            handles_c.append(patch)

        axe.legend(handles=handles_c, loc="upper left", fontsize=16)
        fig.set_size_inches(20, 10)
        plt.xlabel("GDP in $", fontsize=16)
        plt.ylabel("Energy consumption in TWh", fontsize=16)
        plt.title(f"Scatterplot of GDP to Total Energy Consumption in {year} per country*", fontsize=22)
        plt.figtext(0.9, 0.09, "*Size in population", ha="right", fontsize=10)

    def emission_prediction(self, country: str, periods: int) -> None:
        """
        Predicts emissions for a specific country, for a number of periods
        defined by the user. Uses the ARIMA model.

        Parameters
        ----------
        country : str
            Desired country that the prediction should be made for

        periods : int
            Number of periods the prediction should be made for

        """
        # Checking (1) if the inout countrx is a string and in the list of
        # countries and (2) of the period input is an integer
        if not isinstance(country, str):
            raise TypeError("country is not str type")
        if not isinstance(periods, int):
            raise Exception("periods is not int type")
        if periods < 1:
            raise Exception("prediction_periods must be greater than or \
                            equal to 1")

        # Creating a new dataframe with Total consumption and emssions as a
        # coloum and droping 0 values
        df_country = pd.DataFrame({
                                'total_consumption': self.data_frame_filtered[
                                    self.data_frame_filtered[
                                        "country"] == country]
                                .filter(regex="consumption").sum(axis=1)
                                .replace({0: np.NaN}).dropna(),
                                'emissions': self.data_frame[self.data_frame[
                                    "country"] == country]['emissions']
                                .replace({0: np.NaN}).dropna()
                                })
        df_country.index = pd.DatetimeIndex(
            df_country.index.values, freq=df_country.index.inferred_freq)

        # Creating a prediction model for total consumption
        consumption_model = ARIMA(endog=df_country['total_consumption'],
                                  order=(5, 1, 2))
        consumption_results = consumption_model.fit(method_kwargs={
            "warn_convergence": False})
        consumption_predictions = consumption_results\
            .predict(1, len(df_country) + periods)

        # Creating a prediction model for total consumption
        emission_model = ARIMA(endog=df_country['emissions'],
                               order=(5, 1, 2))
        emission_results = emission_model.fit(method_kwargs={
            "warn_convergence": False})
        emission_predictions = emission_results\
            .predict(1, len(df_country) + periods)

        # Plotting both predictions in two seprate plots
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))
        fig.suptitle(f'Energyconsumption and Emission Predictions of {country} for the next {periods} years', fontsize=22)
        ax1.plot(consumption_predictions[
            consumption_predictions.index.year > 2018])
        ax1.plot(df_country['total_consumption'])
        ax1.legend(['Consumption Predictions',
                    'Total Consumption'], fontsize=16)
        ax2.plot(emission_predictions[emission_predictions.index.year > 2018])
        ax2.plot(df_country['emissions'])
        ax2.legend(['Emission Predictions', 'Total Emission'], fontsize=16)
        ax1.set_ylabel('Energy Consumption in TWh', fontsize=16)
        ax2.set_ylabel('CO2 Emissions in Tonnes', fontsize=16)
        ax1.set_xlabel('Years', fontsize=16)
        ax2.set_xlabel('Years', fontsize=16)

    def scatter_emsssion_consumption(self, year: int) -> None:
        """
        Creates a scatter plot for a given year, that plots emissions
        (x-axis) against total energy consumption (y-axis). The size of the
        dots correspond to the population of the respective country.

        Parameters
        ----------
        year : int
            Desired year that the prediction should be made for

        """
        # Creating a new dataframe filterd for the choosen year with the
        # following columns: total consumption, iso_code, country',
        # total_consumption, population, emissions

        df_scatter = self.data_frame_filtered[
            (self.data_frame_filtered.index.year == year) &
            (self.data_frame_filtered['country'] != 'World')]
        df_scatter["total_consumption"] = df_scatter.filter(
            regex="consumption").sum(axis=1)
        df_scatter = df_scatter[["iso_code", 'country', "total_consumption",
                                 "population", "emissions"]]

        # Raise an error if there is not sufficient data in choosen year
        if df_scatter['total_consumption'].sum() == 0:
            raise Exception(f"For picked year {year} is not sufficient data\
                            available")
        if (year > 2016) or (year < 1970):
            raise ValueError("Not data available for that year")

        # Aim is to match each country to a continent to assign each bubble in
        # The gapminder a color corresponding to the continent
        # For that we downloaded a dataset with the contintens per country and
        # Merged it with our orignal on on ISO3 codes to create a new df
        url = "https://raw.githubusercontent.com/datawookie/data-diaspora/master/spatial/country-continent-codes.csv"
        data_frame = pd.read_csv(url,  header=1)

        df_cd = pd.merge(df_scatter.reset_index(), data_frame, how='left',
                         left_on='iso_code', right_on='iso3')
        df_cd['year'] = pd.to_datetime(df_cd['year'], format='%Y')
        df_cd.set_index('year', inplace=True)

        df_cd["continent"].fillna('Other', inplace=True)
        df_cd = df_cd[df_cd['continent'] != 'Other']

        df_cd["area"] = (df_cd['population'] /
                         df_cd['population'].sum())*100000

        # Create a dictionary with continets as keys and colors as values to
        # uniform legend values and colors in scatterplot
        continents = sorted(set(df_cd["continent"]), key=str.lower)
        colors = ['blue', 'green', 'purple', 'red', 'pink', 'orange']
        res = dict(zip(continents, colors))

        # Plot the data
        fig, axe = plt.subplots()
        for i, j in res.items():
            axe.scatter(df_cd[df_cd["continent"] == i]["emissions"],
                        df_cd[df_cd["continent"] == i]["total_consumption"],
                        label=i, alpha=0.5, s=df_cd[df_cd["continent"] == i]
                        ["area"], linewidths=2, c=j)

        axe.set_xscale("log")
        axe.set_yscale("log")
        axe.set_ylim(top=df_cd["total_consumption"].max()*6)

        # Create approptiate legends
        handles_c = []
        for i, j in res.items():
            patch = mpatches.Patch(color=j, label=i, alpha=0.5)
            handles_c.append(patch)

        axe.legend(handles=handles_c, loc="upper left", fontsize=16)
        fig.set_size_inches(20, 10)
        plt.xlabel("CO2 Emissions in Tonnes", fontsize=16)
        plt.ylabel("Energy consumption in TWh", fontsize=16)
        plt.title(f"Scatterplot of CO2 Emissions to Total Energy Consumption in {year} per country*", fontsize=20)
        plt.figtext(0.9, 0.09, "*Size in population", ha="right", fontsize=10)
